# Ln Implementation

This is just to help out with my friends homework. Basically the homework states that to find the natural log of x, we would use a function that would use the power function and we are to find a way to implement the ln(x) function without the power. The full markdown file is listed [here](https://hackmd.io/@imfulee/SkUrNvkb8)

## Intorduction

So, we know that with a function $`ln(x)`$ we could calculate it to the $`n^{th}`$ precision with this function

```math
ln(x) = 2 \sum^\infty_{i=1}[\frac{1}{2i-1}(\frac{x-1}{x+1})^{2i-1}]
```

At least the textbook said so and I'm lazy to verify. Of course, you could just use a Taylor series to figure out the answer but that's not the point. The purpose of this is to figure out the $`n^{th}`$ percision of a $`ln(x)`$ function. 

## Math section

So for explanation purposes we could split up the equation

- Coefficient $`\frac{1}{2i-1}`$
- Alpha part $`(\frac{x-1}{x+1})^{2i-1}`$

Now, in the textbook it writes:

> ...it might be useful to expand series for a few terms to see how the $`\alpha`$ component of a term can be derived from the $`\alpha`$ component of its predecessor...

So let's do excatly that! So when `i >= 1`, the function would look like this:

```math
ln(x) = 2 \times \frac{1}{1}(\frac{x-1}{x+1})^{1}
```

Okay, so what happens when `i >= 2`?

```math
ln(x) = 2 \times (\frac{1}{1}(\frac{x-1}{x+1})^{1} + \frac{1}{3}(\frac{x-1}{x+1})^{3})
```

And then, `i >= 3`?

```math
ln(x) = 2 \times (\frac{1}{1}(\frac{x-1}{x+1})^{1} + \frac{1}{3}(\frac{x-1}{x+1})^{3} + \frac{1}{5}(\frac{x-1}{x+1})^{5})
```

You get the idea. So what kind of pattern do we see?

```math
(\frac{x-1}{x+1})^{3} = (\frac{x-1}{x+1})^{1} \times \frac{x-1}{x+1} \times \frac{x-1}{x+1}
```

And 

```math
(\frac{x-1}{x+1})^{5} = (\frac{x-1}{x+1})^{3} \times \frac{x-1}{x+1} \times \frac{x-1}{x+1}
```

So we could see a pattern that:

```math
(\frac{x-1}{x+1})^{n} = (\frac{x-1}{x+1})^{n-2} \times \frac{x-1}{x+1} \times \frac{x-1}{x+1}
```

Therefore, if you say, save the answer for the `power of 1`, then you could just multiply it by 2 $`\alpha`$ and get the answer of `power of 3`. Similarly, you could get the answer for the `power of 5` by multiplying the answer of `power of 3` by 2 $`\alpha`$ again. **The key to this problem would be to just save the previous answer and multiply it by 2 $`\alpha`$ and you will get the next answer!**

## Code implementation

So in the naive function, written in `C`:

```c
float naiveln(float x, float n)
{
	float ans = 0, alpha = (x-1)/(x+1);
	
	for (int i = 1 ; i <= n ; i++)
	{
		ans += (1.0/(2*i-1))*(pow(alpha, (2*i-1)));
	}

	return 2.0*ans;
}
```

and say the `pow` function is an expensive function to use (somewhat) and we want to get rid of it. So, let's use multiplications instead:

```c
float smartln(float x, float n)
{
	float alpha = (x-1)/(x+1), ans = alpha;
	float save = ans * alpha * alpha;

	for (int i = 2 ; i <= n ; i++)
	{
		ans += (1.0/(2*i-1)) * save;
		save = save * alpha * alpha;
	}

	return 2.0*ans;
}
```
