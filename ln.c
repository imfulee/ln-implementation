/*
Compiler= gcc
C standard>= c99
Compile= gcc ln.c -o ln -lm 
Execute= ./ln
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

clock_t start, end;
float time_used;

float smartln(float x, float n)
{
	start = 0; end = 0;
	start = clock();
	float alpha = (x-1)/(x+1), ans = alpha;
	float save = ans * alpha * alpha;

	for (int i = 2 ; i <= n ; i++)
	{
		ans += (1.0/(2*i-1)) * save;
		save = save * alpha * alpha;
	}
	end = clock();
	time_used = ((float) (end - start)) / CLOCKS_PER_SEC;
	return 2.0*ans;
}

float naiveln(float x, float n)
{
	start = 0; end = 0;
	start = clock();
	float ans = 0, alpha = (x-1)/(x+1);
	
	for (int i = 1 ; i <= n ; i++)
	{
		ans += (1.0/(2*i-1))*(pow(alpha, (2*i-1)));
	}
	end = clock();
	time_used = ((float) (end - start)) / CLOCKS_PER_SEC;
	return 2.0*ans;
}

int main()
{
	float x, n;
	printf("X value:"); scanf("%f", &x);
	printf("N value:"); scanf("%f", &n);
	
	// Checking the values
	if (x <= 0 || n <= 0)
	{
		puts("Invalid arguments");
		return -1;
	}

	printf("Naive ln(x) = %f in %fs\n", naiveln(x, n), time_used);
	printf("Smart ln(x) = %f in %fs\n", smartln(x, n), time_used);

	return 0;
}