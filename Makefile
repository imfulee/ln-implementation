all: ln.o
	gcc -g ln.o -o ln -lm

ln.o: ln.c
	gcc -g -c ln.c

clean:
	rm ln ln.o